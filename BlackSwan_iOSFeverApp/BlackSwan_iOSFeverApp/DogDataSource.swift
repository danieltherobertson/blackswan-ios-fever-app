//
//  DogDataSource.swift
//  BlackSwan_iOSFeverApp
//
//  Created by Daniel Robertson on 23/04/2016.
//  Copyright © 2016 Daniel Robertson. All rights reserved.
//

import UIKit

class DogDataSource: NSObject {
    
    var redditDaata: [String: AnyObject]!

    func getRedditJSON(completion: ([NSURL])->(Void) ){
        var results = [NSURL]()
        let mySession = NSURLSession.sharedSession()
        let url: NSURL = NSURL(string: "https://www.reddit.com/r/dogpictures/new.json")!
        let networkTask = mySession.dataTaskWithURL(url) { (data, res, error) in
            do {
                let jsonResult: NSDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                //If that works, we create an array of dictionaries excepting a string as a key and an any object as its value
                if let redditData = jsonResult["data"] as? [String: AnyObject] {
                    //If that works, we create an array of dictionaries containing the child data of redditData
                    if let children = redditData["children"] as? [NSDictionary] {
                        //Then, we filter through this to remove posts that are stickied. Returns a filtered array
                       let filteredArray = children.filter({ (data) -> Bool in
                            if let childData = data["data"] {
                                if let stickied = childData["stickied"] as? Bool {
                                    return !stickied
                                }
                            }
                            return true
                        })
                        //We then loop through the filtered array, extracting each posts's data object and then each data object's child url (the bit we want)
                        for post in filteredArray {
                            if let data = post["data"] {
                                if let url = data["thumbnail"] as? String {
                                    //Once we have the image URL, we cast it as NSData and append this to the results array
                                    let newUrl = NSURL(string: url)
                                    results.append(newUrl!)
    
                                }
                            }
                        }
                        completion(results)
                    }
                }
            } catch {}
        }
        networkTask.resume()
    }
}
