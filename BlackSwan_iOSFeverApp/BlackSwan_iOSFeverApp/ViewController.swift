//
//  ViewController.swift
//  BlackSwan_iOSFeverApp
//
//  Created by Daniel Robertson on 22/04/2016.
//  Copyright © 2016 Daniel Robertson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var animalCollectionView: UICollectionView!
    @IBOutlet weak var reloadBtn: UIButton!
    
    var dataDelegate = DogDataSource()
    var urls: [NSURL]?
    var images: [NSData]?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.animalCollectionView.delegate = self
        self.animalCollectionView.dataSource = self
        animalCollectionView.backgroundColor = UIColor.clearColor()
        dataDelegate.getRedditJSON { (results) -> (Void) in
            self.urls = results
            
            self.images = [NSData]()
            
            for url in self.urls! {
                if let data = NSData(contentsOfURL: url) {
                    
                    self.images!.append(data)
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.animalCollectionView.reloadData()
                })
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        reloadBtn.layer.cornerRadius = 20
    }
    
    @IBAction func reloadImg(sender: AnyObject) {
        dataDelegate.getRedditJSON { (results) -> (Void) in
            self.urls = results
            
            self.images = [NSData]()
            
            for url in self.urls! {
                if let data = NSData(contentsOfURL: url) {
                    
                    self.images!.append(data)
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.animalCollectionView.reloadData()
                })
            }
        }
    }
    
}



extension ViewController: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images?.count ?? 20
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as! ImageCell

        if images?.count > 0 {
            if indexPath.row < images!.count {
                let imageSlot = images![indexPath.row]
                cell.cellImageView.image = UIImage(data: imageSlot)
            }
        }
        return cell
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {

        return CGSizeMake(130, 130)
    }
}