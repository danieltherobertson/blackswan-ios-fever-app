//
//  ImageCell.swift
//  BlackSwan_iOSFeverApp
//
//  Created by Daniel Robertson on 23/04/2016.
//  Copyright © 2016 Daniel Robertson. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet weak var cellImageView: UIImageView!
    
}
